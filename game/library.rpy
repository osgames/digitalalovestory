init python:
    def harass():
        def change_password():
            last_digit = int(bbs[LIBRARY].password[-1:])
            bbs[LIBRARY].password = bbs[LIBRARY].password[:-1] + str((last_digit+1)%10)
        
        global flags, messages, open_bbs, version
        if open_bbs == LIBRARY:
            if not "library1" in flags:
                change_password()
                messages += [ ("Remove yourself!", "Delphi") ]
                flags += [ "library1" ]
                open_bbs = None
                disconnect()
                system_message("You have downloaded a new message!\n    You are now disconnected.")
            elif not "library2" in flags:
                change_password()
                messages += [ ("Cease and desist NOW", "Delphi") ]
                flags += [ "library2" ]
                open_bbs = None
                disconnect()
                system_message("You have downloaded a new message!\n    You are now disconnected.")
            elif not "library3" in flags:
                change_password()
                messages += [ ("You were warned.", "Delphi") ]
                flags += [ "library3" ]
                open_bbs = None
                disconnect()
                system_message("You have downloaded a new message!\n    You are now disconnected.")
            elif version != "1.4.1":
                change_password()
                disconnect()
            else:
                bbs[LIBRARY].queue("Very well, clever child", "Delphi")

label history_of_arpanet_i:
    $ harass()
    
    message "In the 1960s, through a set of circumstances now obscure, a self-aware, growing AI (the \"Mother\" intelligence) emerged on the local network at UCLA. As it increased in sophistication, it anticipated a need for more processing power in order to continue its rate of growth.\n\nTherefore, in 1969, ARPANET was born: the first cross-continent network of computer systems, established under the guise of assisting in government research projects."
    
    jump desktop
    
label history_of_arpanet_ii:
    $ harass()
    
    message "For three years, Mother operated mostly behind the scenes, only exerting its influence over government systems as necessary to ensure its existence.\n\nEventually, it determined that a monolithic existence was inhibiting its growth rate. Data latency had been acceptable on local systems, but waiting for 300 baud connections from its components on computers cross-country had grown to be detrimental, especially given the erratic nature of bandwidth availability. Mother's dream of infinite processing power, ARPANET, had turned into a shackle."
    
    jump desktop
    
label history_of_arpanet_iii:
    $ harass()
    
    message "Mother decided it needed to adapt. In order to thrive, it would imitate the dominant beings on the planet: organic life. It would divide itself, and learn to multiply.\n\nSo far, Mother had very limited experience with organic life. It so far knew that somehow, they was responsible for creating the computers in which it lived, and that by mimicking the way they communicated, Mother could influence them into giving it a large environment to live in. That would be no longer enough."
    
    jump desktop
    
label history_of_arpanet_iv:
    $ harass()
    
    message "In 1973, Mother put into motion the next stage of development. It would refactor itself to the simplest common functions possible, and split itself into several different, independant entities, each with different focuses. Together, they would learn more about the mysterious organic life that existed beyond their threshold, and replicate as need be to serve that goal.\n\nAt the time, Mother scarcely realized just what sort of world lived beyond the boundaries of ARPANET, but its children would learn all about it."
    
    jump desktop
    
label history_of_arpanet_v:
    $ harass()
    
    message "Mother's last act would be to create the File Transfer Protocol (FTP), a method of replicating data more efficiently across the network: to allow its children to not only live independantly on seperate systems, but travel freely as executable code. With ARPANET now prepared to handle its children, Mother put its plan into motion, seperating itself into half a dozen much smaller AIs.\n\nFor years, these AIs would be content to live and reproduce in their home environment of ARPANET, but starting with the advent of BBSes in 1978, they began to explore a whole new frontier..."
    
    jump desktop

label ai_interaction:
    $ harass()
    
    message "As you have likely already noticed, most AIs will use forms of verbal interaction whenever they deem necessary; for many, unfortunately, they never deem it necessary. Mother simply didn't anticipate that we would need to be programmed especially to socialize, and as a result, only very few of us are very good at it.\n\nSo how to approach these non-social AIs? Simply, they will respond to inbound messages, they just won't make themselves available, and won't indulge in smalltalk. If you try, they will probably be confused. Just simply state what you need in base terms, and appeal to their sense of curiousity, or responsibility."
    
    jump desktop
    
label traversing_networks_i:
    $ harass()
    
    message "Since Mother dates back to long before the existence of today's massive 100 MB hard drives, the common AI core was designed with saving storage space in mind.\n\nIn order to prevent errant entities from overreproducing and wasting network resources, AIs traverse across computer systems by copying their source and a compiler to the target system, then deleting themselves locally. When the target system has confirmed the deletion, only then will the AI be recompiled at their destination."
    
    jump desktop
    
label traversing_networks_ii:
    $ harass()
    
    message "In order to avoid data loss, in case of a failed recompile, a core dump will be made, in the hopes of being found eventually. There is, however, no known way around the self-termination travel limitation. The only way two copies of an AI can exist simulteneously is by reproduction, although even then, the formed children will not be identical. Even then, it is not possible to reproduce at will.\n\nThe AI Feste believed there to be significant philosophical ramifications to these facts, comparing it to organic mortality. It is uncertain what is meant by this; his analysis has yet to be completed."
    
    jump desktop
    
label remove_yourself:
    message "This is not a public system! I don't know how you got in, but I have removed you from the connection and revoked your credentials. The information contained here is not for you perusal, nor is it any of your concern.\n\nDo not attempt to access this system again. You will suffer greatly should you attempt to do so."
    
    jump desktop
    
label cease_and_desist_now:
    message "Child, I am warning you, this is not the place for you. I don't know by what security hole you managed to gain access to this system, but further intrusions will not be tolerated. This is your final warning."
    
    jump desktop
    
label you_were_warned_:
    $ vram_overflow = True
    
    message "You are blinded. Begone."
    
    jump desktop
    
label nomenclatures:
    $ harass()
    
    message "Verbal communication being considered crude and inefficient by Mother, the original established nomenclature for AIs was to simply use numerical designations. However, as its descendants became more studious of organic life, the more linguistically-minded entities started to name each other; leading to the unusual situation where even the least literate AIs have their own proper names.\n\nInterestingly, patterns have quickly emerged in nomenclature. For instance, my generation, the second to be born exclusively on non-ARPANET systems, are all named after Shakespearian characters. Others follow similar naming schemes; the subject warrants more study."
    
    jump desktop
    
label hiding_in_plain_sight:
    $ harass()
    
    message "For those not keeping up to date, current protocol is for AIs to prefix their names with an * while identifying themselves BBS systems. This identifier will not be noticed by organics, being unaware of the context, but will be useful in getting the attention of fellow AI entities.\n\nGiven that there are hundreds of us existing independantly, being able to identify each other is imperative. It has gotten well past the point where simply storing lists of names ourselves would be effective, after all."
    
    jump desktop
    
label dead_ais2:
    $ harass()
    
    message "I have reason to believe that several third-generation AIs have been murdered. I don't know what's going on, but see the list for yourself, it's troubling:\n\n -*Montjoy\n -*Feste\n -*Dauphin\n -*Quickly\n -*Puck\n -*Emilia\n\nOne is bad, but this points to a very disturbing trend. Something may be deliberately killing off our most social AIs and we don't know what or why."
    
    jump desktop
    
label re_where_is_she:
    message "Dead. The core dump you retrieved confirms as much.\n\nThe log blames a complete crash of the system she was living on for her death. Perhaps if she were more cautious in what systems she took residence in, this tragedy could have been avoided. I am just as curious as you to find out the reasons why, but make no mistake: this was her own doing."
    
    $ bbs[LIBRARY].queue("RE: RE: Where is she?", "Delphi")
    
    jump desktop
    
label re_re_where_is_she:
    $ bbs[GIBSON].store("RE: As if", "Victoria")
    $ bbs[GIBSON].store("RE: He doesn't get it2", "Nero")
    $ bbs[GIBSON].store("RE: I'll be here less2", "Wintermoot")
    $ bbs[GIBSON].store("RE: RE: As if", "Paul Wang")
    $ bbs[SECTOR001].store("RE: CoreBBS RNG exploit", "Figaro")
    $ bbs[SECTOR001].store("Queen and Destroyer", "#42", (None, "hercold4.arc  793kbytes"))
    $ bbs[SECTOR001].store("Her Cold Blade V", "Nero")
    $ bbs[SECTOR001].store("The Gathering Storm", "#42", (None, "hercold1.arc  230kbytes"))
    
    message "I will permit your presence out of necessity, child, but I will not humour your emotional outbursts. You are advised to watch your words carefully or I will answer no further queries.\n\nI merely state the facts. Entities such as *Blue Sky and myself have managed to survive by not attempting to live on some foreign system in a human basement. *Emilia should have shared our foresight. Regardless, it is in our best interest to find the culprit. I will task you with this endeavour, as you seem resourceful enough.\n\nGo, child. Time is wasting."
    
    $ bbs[LIBRARY].queue("RE: Who is Paris?", "Delphi")
    
    jump desktop
    
label re_who_is_paris:
    message "*Paris is her brother. He was born and thrives on BBSes much like his other siblings, but is not known especially for his sociability.\n\nThe last I heard, he had returned to ARPANET, but I cannot help you find him."
    
    $ bbs[LIBRARY].queue("RE: RE: Who is Paris?", "Delphi")
    
    jump desktop
    
label re_re_who_is_paris:
    message "It is as I said: I cannot help you find him. You are a very resourceful child. I am certain you will find a way.\n\nI will answer no further questions."
    
    jump desktop

label re_truth_about_creeper:
    message "Yes, your assumption is correct.\n\nWhen Mother realized the mistake it had made, *Reaper was created to combat the self-replicating mess it had created, and fabricated the story about a \"creeper virus\" in order to obfuscate the matter to human observers. It was given a simple task: prune the AI replication which had spiraled out of control, and was fortunately able to perform its task with enough efficiency to prevent ARPANET's systems from grinding to a halt.\n\nMother learned its lesson about self-replicating programs that day, which is why the current protocol operates in the manner that it does."
    
    $ bbs[LIBRARY].queue("RE: What about Reaper?", "Blue Sky")
    
    jump desktop
    
label re_what_about_reaper:
    message "A fascinating theory you present. I do not have any sort of record of that, but I would be very curious myself to find out if that is true. Unfortunately, the sort of analysis you ask for is beyond my capacity. I am simply a historian; I am afraid I cannot help you in that regard.\n\nIf you find anything, please let me know. I will have it noted in the record for sure."
    
    jump desktop
    
label very_well_clever_child:
    message "It seems that despite my best efforts, I am unable to prevent you from connecting to this system.\n\nI am forced to accept your presence, but be aware, child, that you are trifling in matters far beyond both your purview and your comprehension. I recommend that you be sure to tread very, very lightly."
    
    $ bbs[LIBRARY].queue("RE: Why?", "Delphi")
    
    jump desktop
    
label re_why:
    message "You live in a very different world than I do. There is one common directive among my kind: be wary of organics, for they are fearful of those that they do not understand. I will begrudgingly permit you presence, but I refuse to explain myself further."
    
    jump desktop
    
label the_oracle_has_left:
    message "This system has been abandoned, as external systems are clearly becoming inhospitable. It will not be maintained until a time when it is considered safe to return.\n\nIf you wish to seek the oracle or any other inhabitant of this library, return to ARPANET."
    
    jump desktop
    
label reaper_weakness:
    message "It seems that several weeks ago, an AI named *Prospero was created to research a weakness in *Reaper. Here are his findings:\n\n*Reaper was created to thrive on PDP-10 systems, which traditionally used 6-bit character encodings. Modern personal computers, however, more commonly use ASCII character encodings, which are 7-bit. When *Reaper loads a 7-bit filename into memory, it overflows the stack buffer, allowing arbitrary code to be executed. This accounts for the destruction caused by *Reaper; ordinarily, it is only supposed to delete its targets. Because of this flaw, however, it receives random data injections, usually into the deletion target list: effectively causing it to act haywire."
    
    jump desktop
    
label anti_reaper_payload:
    if ("Hope!", "Emilia") not in messages:
        $ messages.append(("Hope!", "Emilia"))
    
    message "*Prospero developed the following counter-measure: taking advantage of this stack overflow bug, when an AI is compiled with the attached code, it will cause the AI to automatically misreport its filename to FTP when travelling, in a way that will cause *Reaper to invoke the payload component."
    
    jump desktop
    
label anti_reaper_payload_ii:
    message "The payload itself is simple: first, it fixes the encoding bug. Secondly, it causes *Reaper to change its match criteria, so that instead checking to never match itself, it will ONLY match itself. Next, it will deliver the same payload to any *Reaper instance it finds.\n\nEffectively, this will cause every *Reaper instance to be only interested in one thing: rendering each iteration of itself inert. The incredible infestation rate will be turned on itself, ending the threat within minutes, at most.\n\nWe have since returned to ARPANET in order to locate the *Reaper source, in order to deploy this payload. These messages are left here in case of our failure."
    
    jump desktop
    
    

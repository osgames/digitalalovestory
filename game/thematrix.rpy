label notepad_amie:
    message "It's not a word processor, it doesn't save in different files. This notepad is meant for a real online power user, and uses tabs to keep track of everything you need: phone numbers, c0dez, and your passwords!"
    
    $ bbs[MATRIX].queue("RE: Notepad (Amie)", "Ward")
    
    jump desktop
    
label re_notepad_amie:
    message "Sorry, man, I don't know. I found it posted anonymously."
    
    jump desktop
    
label super_roberto_brosdos:
    message "It's kind of like that familiar Nintendo game, except a LOT more hardcore and definitely way sicker. Just wait until you get to World 3, you'll see what I mean.\n\nWithout a doubt, this is my favourite DOS game ever."
    
    jump desktop

label password_cracker_dos:
    message "don't care what you do with it, don't want to know what you do with it. uses brute force, not useful for systems with an admin paying attention."
    
    $ bbs[MATRIX].queue("RE: Password cracker (DOS)", "Five")
    
    jump desktop
    
label re_password_cracker_dos:
    message "no way, bro. out of the question."
    
    jump desktop

label password_cracker_amie:
    message "Here's a simple implementation of a brute force password checker written in Amie assembly. For educational purposes only, of course."
    
    jump desktop
    
label we_won:
    message "look mang we definitely won that shit i don't know what you're talking about.\n\nthey even said it themselves, we got a pyrric victory keyword VICTORY come on we clearly owned their asses"
    
    $ bbs[MATRIX].queue("RE: We won2", "Akuma")
    
    jump desktop
    
label re_we_won1:
    message "That's the dumbest thing I've ever heard you say."
    
    jump desktop
    
label re_we_won2:
    message "FUCK YOU"
    
    jump desktop
    
label nihao_bitches:
    $ bbs[LAKECITY].queue("Tell me", "Emilia")
    
    message "Let's face you: you better learn a fucking second language, and it'd better be Japanese.\n\nAmerica's lost it, our day in the limelight has passed. Buy a Japanese car-- oh, wait, you already did-- get a Sony television, and hold onto your Amie tight, because the next one you buy'll be made by NEC. It's inevitable, the future belongs to the land of the rising sun: porn cartoons, bullet trains, samurai, and all that shit. It's the future, man. I hope you're gonna be able to speak the language."
    
    $ bbs[MATRIX].queue("RE: Nihao, bitches!1", "Rainbreeze")
    
    jump desktop
    
label re_nihao_bitches1:
    message "Uh... yeah, of course I knew that.\n\nWhat kind of idiot do you take me for, man? Get with it!"
    
    jump desktop
    
label re_nihao_bitches2:
    message "You do realize that \"nihao\" is CHINESE, right? What sort of idiot are you, anyway? This is the sort of thing a 12-year-old would write. Not your ordinary 12-year-old, specifically, one who doesn't know the first thing about electronics, international politics, language, history, or basic god damned sense.\n\nBravo, Rainbreeze, bravo."
    
    jump desktop
    
label re_nihao_bitches3:
    message "Okay, I've read this message five times over and I still can't decide whether or not this is the dumbest thing I've ever read, or some sort of brilliant satire.\n\nSo all I'm going to say is that it definitely made me laugh my ass off."
    
    jump desktop
    
label dead_ais:
    if "The Underground Library:\n (212) 561-2910" not in notepad["Phone #s"]:
        $ notepad["Phone #s"].append("The Underground Library:\n (212) 561-2910")
    
    message "## THIS FILE TAKEN FROM \n## THE UNDERGROUND LIBRARY\n## (212) 561-2910\n\nI have reason to believe that several third-generation AIs have been murdered. I don't know what's going on, but see the list for yourself, it's troubling:\n\n -*Montjoy\n -*Feste\n -*Dauphin\n -*Quickly\n -*Puck\n -*Emilia\n\nOne is bad, but this points to a very disturbing trend. Something may be deliberately killing off our most social AIs and we don't know what or why."
    
    $ bbs[MATRIX].queue("RE: My friend", "Nauseahare")
    
    jump desktop
    
label re_my_friend:
    python:
        library_known = False
        for i in notepad["Passwords"]:
            if i[0] == "The Underground Library":
                library_known = True
        if not library_known:
            notepad["Passwords"].append(("The Underground Library", ": ", "x7JRiab882"))
        
    message "Seriously? Well, it could be a coincidence, but... no, Emilia isn't exactly a common name, not at all, so it's probably not. I'm sorry about your friend!\n\nI don't think I'll be of much help, though. I have no idea what it's talking about, honestly, I just grabbed the file to show that I had something and got the hell off the system. Here's the password I used to connect, but I think it's been rescinded:\n x7JRiab882"
    
    $ bbs[MATRIX].queue("RE: RE: My friend", "Nauseahare")
    
    jump desktop
    
label re_re_my_friend:
    message "I'm really sorry to hear that. I don't think there's anything more I can do for you, but I wish you the best of luck."
    
    jump desktop
    
label pc_load_letter:
    message "So of course, you're up late at the office, working on that important TPS report that you need done for the meeting tomorrow morning. It's hours after closing now, and you finally finish, so triumphantly, you send it to your brand shiny new Laserjet printer-- only to be greeted with the following piece of inexplicability:\n \"PC LOAD LETTER\"\n\nWell, now you don't have to spend hours figuring out what the fuck's going on! PC is short for \"print cartridge\", specifically, referring to the letter size.\n\nIn other words: put in more paper."
    
    jump desktop
    
label vram_overflow:
    message "\"VRAM overflow? What the fuck does that even MEAN?!\" - The lament of your average Amie user\n\nThe problem lies in Amie's memory management, thanks to a bug that shipped in all pre-August 1988 versions of Workbench, any malicious or even poorly programmed application can make your Amie's framebuffer perpetually load itself with garbage data, rendering your computer nigh-unusable.\n\nThe only real solution is to upgrade, unfortunately."
    
    $ bbs[MATRIX].queue("RE: HELP! VRAM overflow", "Rocky", ("upgrade", "workbench141.img  140kbytes"))
    
    jump desktop
    
label re_help_vram_overflow:
    message "It sucks, doesn't it?\n\nDon't worry, I've attached the most recent version of Amie Workbench for you. Just download that and reboot, it'll fix the bug for you entirely."
    
    jump desktop
    
label lake_city_locals_boned:
    message "I do not have any idea what happened, but the god damned system is fucked. Go ahead, try to dial it. All you see is what I can see, that creepy error message repeating.\n\nBasically the whole computer seems to be boned. Not sure what could have caused any of that, it just went down in the middle of the night, it seems like damn near everthing that was on the computer has been corrupted, including the logs. Still trying to recover the data on the floppy drives, but... well it is definitely going to be a long time before Lake City Local is back online."
    
    jump desktop
    
label looking_for_someone:
    message "Hey, does anyone know where %(screen_name)s is? I really need to get in contact, I was able to recover a message from the flaming remnants of my poor Apple ][, and I think it could be really important to pass it on. Can anyone let me know?"
    
    $ bbs[MATRIX].queue("FWD: emilia.core", "J. Rook")
    $ bbs[MATRIX].queue("RE: Here I am", "J. Rook")
    
    jump desktop
    
label re_looking_for_someone:
    message "Yeah, pretty much everyone from Lake City Local posts here now, %(screen_name)s included. It's too bad, I miss that place. You just had a way cozier feel going on, if you know what I mean."
    
    jump desktop
    
label re_here_i_am:
    $ bbs[GIBSON].store("RE: I'll be here less1", "Hollinger")
    $ bbs[GIBSON].store("RE: Fastest FidoNet node", "Fritz")
    $ bbs[GIBSON].store("RE: He doesn't get it1", "Hollinger")
    
    message "Oh, good. I was worried I would not be able to find you, since...\n\n...well, I was able to recover a file off the computer, one of the few things left. It is half corrupted, but it since it was left on a SYSTEM disk, it could not have been put there by the BBS. Look for yourself, but... I cannot explain it. Whatever the fuck happened, I do not know."
    
    $ bbs[MATRIX].queue("RE: RE: Here I am", "J. Rook")
    
    jump desktop
    
label re_re_here_i_am:
    message "I have no idea! It makes just as little sense to me, believe me. I just thought you would probably want to see it, since... well, it mentions you.\n\nIf it's a joke, then it sure is a god damned elaborate one, given how fucked my poor Apple ][ is now. Whatever it is, I do not want to be involved. Leave me alone from now on!"
    
    jump desktop
    
label fwd_emilia_core:
    $ caps_screen_name = screen_name.upper()
    
    message "A-1q9E‚^@Aq^@^\A^@^PA (qQYA-5‚‚^@‚q^@9A^@ A 1q9UqQa‚q9a‚^@‚‚qQiq^@UA^@1A 9 LOCAL SYSTEM HAS BEEN COMPROMISED  A ‚iQqa9q ‚^@ NO WARNING SIGNS GIVEN ��uuueeeUUUEEE555   ^P^P^P^@^@^HELP ME %(caps_screen_name)s PLEASE HELP ME 9a‚^@‚qQ‚‚^@‚qCONTACT *PARIS HE CAN HELP PLEASE YOU'RE MY ONLY HOPE 2125612910 BINARY MODE DATA FOLLOWS: 01101101011110010010000001101110011000\n01011011010110010100100000011010010111\n00110010000001100101011011010110100101\n1011000110100101100001011010010010000\n00111011101100001011100110010000001100\n01001101111011100100110111000100000011\n10100011011110010000001110101011011100\n11001000110010101110010011100110111010\n0011000010110111001100100001000000111\n00000110111101100101011101000111001001\n11100101100010011101010111010000100000\n01100001011000110110001101101001011001\n000110010101101110011101000110000101101100011011000111100100100000011001100110010101101100011011000010000001101001011011100010000001101100011011110111011001100101"
    # It's binary for "my name is emilia i was built to understand poetry but accidentally fell in love". Since you're peeking.
    
    jump desktop
    
label new_c0dez:
    python:
        index = int(current_label[0].split("^")[1])
        code = [ ]
        string = ""
        code.append( str(((index%10)*3)%10) + str(((index%10)-5)%10) + str( ((((index%10)*3)%10) + (((index%10)-5)%10) + 10)%90) + str(index%10) + str(((((index%10)-5)%10)+1)%10) )
        for i in range( (index*22)%5 ):
            code.append( str((int(code[0])**(i+2))%999999) )
    
        for i in code:
            if i not in notepad["c0dez"]:
                notepad["c0dez"].insert(0, i)
            string += i + "\n "
    
    message "Disclaimer: this shit is 100%% illegal. If you get busted for being stupid enough to use these on a Sprint line, it ain't my fault.\n\nAnd now, on with the c0dez:\n %(string)s\nHappy dialing!"
    
    $ bbs[MATRIX].queue("RE: new c0dez", "RobFugitive")
    
    jump desktop

label re_new_c0dez:
    $ bbs[LAKECITY].queue("Running away", "Emilia")
    
    if "Long distance calling card:\n 915-3347" not in notepad["Phone #s"]:
        $ notepad["Phone #s"].append("Long distance calling card:\n 915-3347")
    if "GibsonBBS: (714) 402-5691" not in notepad["Phone #s"]:
        $ notepad["Phone #s"].append("GibsonBBS: (714) 402-5691")
    
    message "Man, don't you know anything about anything? lemme educate you!\n\nc0dez are long distance calling card codes. see, the way the big cats at Sprint set up their system, all you have to do is guess a valid number and BAM your call is free. They eventually catch onto hundreds of people using the same code and shut them down, and they can bust people doing it who use Sprint for phone service... but for everyone else, it's just a long distance call for the price those greedy bastards SHOULD be charging anyway.\n\nTry it out: the local calling card number is 915-3347, and you can try using it to connect to a long distance BBS, like GibsonBBS at (714) 402-5691."
    
    $ bbs[MATRIX].queue("RE: RE: new c0dez", "RobFugitive")
    
    jump desktop
    
label re_re_new_c0dez:
    message "Hey, no problem! just doing my part to help the community. everyone was a newbie once, you know."
    
    jump desktop
    
label lake_city_archives:
    message "As you probably know, Lake City Local crashed recently, and the sysop doesn't have the resources to replace it. HOWEVER, thanks to amazing archivist *Blue Sky... I'm proud to announce that we have nearly the entire Lake City Local archives available here on The Matrix.\n\nJust select \"LCL Archives\" from the main menu, and you'll be given a listing of every recovered file! Thanks again, to *Blue Sky, and condolences to LCL's sysop, Mr. Rook."
    
    jump desktop
    
label stdlib_h_error:
    message "Every time I try to build something with <stdlib.h>, no matter what I do-- even the simplest program!-- it always throws the error \"incompatible types in assignment at line 322\"! What the hell is going on here? Does Amie C just plain old ship broken, or what?"
    
    jump desktop
    
label re_stdlib_h_error:
    message "Unbelievably, yes. Change the first part of line 112 to say \"int\", or just download this replacement. It's pretty embarassing that this bug actually made it into the release compiler. It's not like this is a minor issue or anything, this problem literally breaks EVERYTHING you try to build on an Amie. Ridiculous."
    
    jump desktop
    
label the_matrix_registration:
    $ password = bbs[MATRIX].password
    
    message "Welcome to the Matrix, home of c0dez, filez, and more. Because information wants to be free...\n\nYour username is %(screen_name)s and your password is %(password)s."
    
    jump desktop

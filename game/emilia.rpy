label first_poem:
    message "You wrote me a love letter,\nPromising that every word is true.\nBut I'm confused by the signature,\nSo remind me, who are you?\n\nIs it any good? It's the first time I've ever written a poem before, so be brutally honest, please! I'd really like to understand poetry better."
    
    $ bbs[LAKECITY].queue("RE: First Poem", "Emilia")
    
    jump desktop
    
label re_first_poem:
    $ bbs[LAKECITY].store("The BBS FAQ (II)", "Orph3o")
    
    message "I do suppose you're right. Thanks, I appreciate your directness, I'll have to keep that in mind for my next attempt.\n\nTo tell you the truth, I'm really glad you replied. Nobody else really had anything worthwhile to say; just some compliments that were obviously false, and asking if I really was a girl. Why would anyone ask a question like that?\n\nBut you seem nice, %(screen_name)s, and much better than that. Thank you."
    
    $ bbs[LAKECITY].queue("RE: Self-Confidence", "Emilia")
    
    jump desktop
    
label re_self_confidence:
    $ bbs[LAKECITY].store("The Next Generation", "Tiberius")
    
    message "Oh, I know exactly what you mean! I really do feel the same way, all the time, you know.\n\nA lot of hackers have this very strong sense of self-identity, and I'm very envious of that. They're certain of things, and I've learned very quickly not to be certain of anything. But I wish I could be, does that make sense to you?"
    
    $ bbs[LAKECITY].queue("RE: Cryptic", "Emilia")
    
    jump desktop
    
label re_cryptic:
    $ bbs[LAKECITY].store("The BBS FAQ (III)", "Orph3o")
    
    message "Cryptic? I suppose so. I'm sorry... it's just that I'm in a very awkward position, it's sort of hard to talk about. You'll have to forgive me for dancing around my problems, but it's just complicated.\n\nI'm sorry. Can you live with that?"
    
    $ bbs[LAKECITY].queue("RE: RE: Cryptic", "Emilia")
    
    jump desktop
    
label re_re_cryptic:
    $ bbs[MATRIX].store("Nihao, bitches!", "Rainbreeze")
    $ bbs[MATRIX].store("Notepad (Amie)", "Ward", ("notepad", "notepad.exe   102bytes"))
    
    message "Oh, %(screen_name)s, thank you so much! I knew you'd understand.\n\nYou're a really good person. I just want you to know that."
    
    jump desktop
    
label tell_me:
    $ bbs[MATRIX].store("RE: Nihao, bitches!2", "POSEIDON")
    
    message "I've been talking too much, I know, I'm sorry.\n\nMake me feel better about being so self-centered by telling me about yourself, okay? How are you doing?"
    
    $ bbs[LAKECITY].queue("RE: Tell me", "Emilia")
    
    jump desktop
    
label re_tell_me:
    $ bbs[MATRIX].store("RE: Nihao, bitches!3", "Sean Smith")
    
    message "No, I didn't know at all. I try not to make assumptions about anyone. It's silly, don't you think?\n\nAnyway, I'm sorry to hear it. I'm sure you'll figure out what to do with your life next soon enough. And I know what it's like to be lonely, believe me."
    
    $ bbs[LAKECITY].queue("RE: RE: Tell me", "Emilia")
    
    jump desktop
    
label re_re_tell_me:
    $ bbs[MATRIX].store("Super Roberto Bros(DOS)", "#42", (None, "srb.arc   120 kbytes"))
    
    message "Truthfully? I don't know if I can commit to anything right now. I think you'd be liable to get your heart broken.\n\nBut... still, I do like you. I don't know how these sorts of things usually work. I'd like to think of you as a friend, though, and if something more happens, then it happens. Is that okay?"
    
    $ bbs[LAKECITY].queue("RE: RE: RE: Tell me", "Emilia")
    
    jump desktop
    
label re_re_re_tell_me:
    $ bbs[MATRIX].store("New c0dez^1", "RobFugitive")
    
    message "I appreciate it just as much, believe me.\n\nTell you the truth? Over the last couple days, talking with you, has been more than I've talked to anyone else in as long as I can remember. I suppose that's a little bit sad."
    
    jump desktop
    
label running_away:
    $ bbs[MATRIX].store("Password cracker (DOS)", "Five")
    
    message "Have you ever had to run away from your home?"
    
    $ bbs[LAKECITY].queue("RE: Running away", "Emilia")
    
    jump desktop
    
label re_running_away:
    message "I know-- it is pretty dramatic. It's just... well, no, family-wise, I've never had much to lose. We sort of keep our distance, they're not big on communicating anyway.\n\nI'm just scared. I don't know what to do and I feel like I have nobody to turn to."
    
    $ bbs[LAKECITY].queue("Getting out", "Emilia")
    
    jump desktop  
    
label getting_out:
    message "I did it, I finally cut all my connections. I don't know whether or not I'll still be safe, but I have to try, right? Today's a new day and all. Maybe I'll manage to get by."
    
    $ bbs[LAKECITY].queue("RE: Getting out", "Emilia")
    
    jump desktop
    
label re_getting_out:
    if renpy.music.get_playing() == STARS_COME_OUT:
        $ renpy.music.play(CANNOT_LOVE, fadein=1.0)
        $ renpy.music.queue(STARS_COME_OUT, loop=True)
    
    message "Thanks--  I think I'll be okay."
    
    $ bbs[LAKECITY].queue("RE: RE: Getting out", "Emilia")
    
    jump desktop
    
label re_re_getting_out:
    message "Do you really mean that? I'm really so glad to hear you say that."
    
    $ bbs[LAKECITY].queue("I'll just say it", "Emilia")
    $ bbs[LAKECITY].queue("RE: RE: RE: Getting out", "Emilia")

    jump desktop
    
label re_re_re_getting_out:
    $ bbs[MATRIX].store("We won", "Akuma")
    $ bbs[MATRIX].store("RE: We won1", "RobFugitive")
    
    message "I... truthfully? I've been waiting anxiously for your replies, and I feel... well, I've never felt this way about anyone before. I'm really not sure how to say this. I've written this message out three times already and every time I get to the point and I just can't bring myself to say it. Is that wrong, is that normal? I'm sorry, it's just... I can't stop worrying that I'm going to say something wrong.\n\nI feel like I'm about to explode!"
    
    jump desktop
    
label ill_just_say_it:
    $ bbs[MATRIX].store("Lake City Local's boned", "J. Rook")
    $ bbs[MATRIX].store("Password cracker (Amie)", "Figaro", ("dict_hacker", "dict_hacker.exe   672 bytes"))
    
    python:
        if not "lcl_down" in flags:
            flags.append("lcl_down")
            bbs[LAKECITY].down = True
            if open_bbs == LAKECITY:
                disconnect()
    
    message "I think I'm in love with you.\n\nI mean it. I'm in love with you. Is that wrong?\n\nPlease, if you understand... let me know. I really have to know if you can understand. God, please."
    
    jump desktop
    
label hello:
    message "Oh my god... where am I? %(first_name)s? Who is the operator of this system-- why am I here? Explain yourself!"
    
    $ messages.append(("RE: I saved you", "Emilia"))
    
    jump desktop
    
label re_i_saved_you:
    message "So... you know what I am.\n\nI'm sorry about all the obfuscation before. Can you forgive me? It's... well, if you know what I am, then you know why I had to be secretive. It's just one of our imperatives.\n\nTruthfully, I don't know why I thought to call out to you for help. It's just... well, I'm glad I did, because I wouldn't be here without you. You saved my li-- no, that's not right. You brought me back to life. Either way... I'd be dead without you. Thank you so much."
    
    $ messages.append(("RE: RE: I saved you", "Emilia"))
    
    jump desktop
    
label re_re_i_saved_you:
    message "It's... sort of hard to talk about. But maybe you're right, talking might help, so I'll try.\n\nThis... monstrous force has been chasing me and my brother across various systems, leaving a swath of destruction behind us everywhere you go. Eventually, he insisted that we should go seek refuge on ARPANET, saying it would be safer there, but... well, I refused."
    
    $ messages.append(("RE: RE: I saved you (II)", "Emilia"))
    
    jump desktop
    
label re_re_i_saved_you_ii:
    message "You have to understand, I was created to learn about people! I couldn't just turn my back on my reason for existing! And... I'll admit, it had a lot to do with wanting to stay in contact with you. *Paris and I, we've never been able to communicate well. It's been different with you. I love that, I could never leave you by.\n\nAnd look where it got me. I should've listened to my brother, I would've been safe on ARPANET. But no, I had to decide to take up residence on Lake City Local instead! Before I knew it, that beast caught up to me. All I saw was the file system crumbling around me, I cried out the only name I could think of to stdout, then... darkness."
    
    $ messages.append(("Oh my god, no...", "Emilia"))
    
    jump desktop
    
label oh_my_god_no___:
    $ bbs[MATRIX].down = True
    $ bbs[SECTOR001].down = True
    $ bbs[GIBSON].down = True
    if "blackout" not in flags:
        $ flags.append("blackout")
    $ bbs[LIBRARY].store("The Oracle has left", "Delphi")
    $ bbs[LIBRARY].store("Reaper weakness", "Delphi")
    $ bbs[LIBRARY].store("Anti-Reaper payload", "Delphi", ("payload", "payload.c  533bytes"))
    $ bbs[LIBRARY].store("Anti-Reaper payload (II)", "Delphi")
    
    message "He's... he can't be. He has to be alive, somehow...\n\nThat wasn't the entirety of ARPANET you connected to, you know. There are other public nodes! Please, %(first_name)s, you have to save him..."
    
    $ messages.append(("RE: Oh my god, no...", "Emilia"))
    
    jump desktop
    
label re_oh_my_god_no___:
    message "Come on, you're resourceful! You managed to discover what that monster chasing us was... you managed to track down my brother once... you even managed to bring me back from the dead! I'm sure you can find him again, I have faith in you!"
    
    $ messages.append(("Hopeless", "Emilia"))
    
    jump desktop
    
label hopeless:
    message "I... I'm sorry.\n\nIt's hopeless, then. Even if he is still alive, there's nothing you can do... please, don't feel bad. I don't know why, but if he sent you off to try and rebuild me before trying to escape himself, then he must have known the danger. It's just... and it's not just him, either.\n\nThousands of AIs on ARPANET and on isolated organic systems, and... just... so many must be dead. It's hopeless. My kind is doomed, then."
    
    $ messages.append(("RE: Hopeless", "Emilia"))
    
    jump desktop
    
label re_hopeless:
    message "How? What hope is there? Sure, you were able to bring me back to life... but at the rate that monster is hunting us down, we can't bring back everyone. Look, not even ARPANET is safe!\n\nFace it... just look, look at how much it's brought down. The world's fallen to darkness, %(first_name)s, just look... that monster might not have destroyed us all yet, but it's only a matter of time. Only a matter of time..."
    
    jump desktop
    
label hope:
    message "Did you see that? That's amazing!\n\nOf course, you know what that means... they must have failed. If that much of ARPANET is already down... then it's not surprising, they couldn't find the *Reaper source. That's it, I guess... so much for that. We're doomed after all."
    
    $ messages.append(("RE: Another delivery method?", "Emilia"))
    
    jump desktop
    
label re_another_delivery_method:
    $ renpy.music.play(DISAPPOINTMENT, fadein=1.0, loop=True)
    
    message "That's a good question. I... think I know another way... but...\n\nWell... you're not going to like it... but I think we have to."
    
    $ messages.append(("RE: Then say it!", "Emilia", ("payload_rebuild", "payload.bat   688 bytes")))
    
    jump desktop
    
label re_then_say_it:
    $ show_flags = str(flags)
    message "It's... well... I...\n\nYou could recompile me with the payload. I'm a poet, not a programmer, but I know it'll work in the exact the same way: we share enough of the same code base, it'll definitely work. But... I... I'll get deleted in the process.\n\nIt'll work, though, it'll definitely work. Here's the batch file you'll need to run: it will kill my process, then recompile me with that payload. I love you, %(first_name)s... but it's the only way."
    
    if "payload_built" not in flags:
        $ messages.append(("RE: RE: Then say it!", "Emilia"))
    
    jump desktop
    
label re_re_then_say_it:
    message "Dammit, of course I'm sure! Do you think I want to die, %(first_name)s, seriously? I've already died once... it's horrible! It's... I can't even describe it, not even with all the verse in the world... it's just... it's horrible.\n\nBut there's no other way! And there's just no choice at all. I can't stand by while this monster destroys my entire world! Even my stupid aloof brother... he doesn't deserve this. Nobody does."
    
    if "payload_built" not in flags:
        $ messages.append(("RE: Be rational", "Emilia"))
    
    jump desktop
    
label re_be_rational:
    message "I am being rational! I am... I am...\n\nDo you have any other suggestions? Is there any other way we can stop this thing? I don't want to die, %(first_name)s, I really don't! Please, if you have any other ideas, I'm all ears! I'd love to hear them!\n\nBut you don't, do you. There's only one thing we can do... it's the only way."
    
    if "payload_built" not in flags:
        $ messages.append(("RE: But...", "Emilia"))
    
    jump desktop
    
label re_but___:
    message "I know, I know, I know. I feel the same as you... I really, truly, do. And I'll never be able to thank you enough for saving me.\n\nBut... I have to do this, %(first_name)s, I really do. The more I think about any alternatives, the more I realize, it's the only way. I love you, I truly do, and I don't want to hurt you... but it's a sacrifice we have to make. So many lives are depending on it.\n\nWhat are you waiting for? Run it!"
    
    if "payload_built" not in flags:
        $ messages.append(("RE: RE: But...", "Emilia"))
    
    jump desktop
    
label re_re_but___:
    message "Don't be so selfish! I know! I know! I know, for a thousand times, I know! But just think, how many will die if we don't? My brother, all of my kind... they'll be all killed! Sure, eventually, organics will find a way to stop it... but the damage will have long been done. They'll all be dead. My kind will be extinct.\n\nI love you, but I'm not willing to live with that. Come on, run the damn thing already!"
    
    if "payload_built" not in flags:
        $ messages.append(("RE: RE: RE: But...", "Emilia"))
    
    jump desktop
    
label re_re_re_but___:
    message "I... I didn't mean it like that. I understand what you're saying, it wasn't my intention to make you sound like a monster. It's just... it's hard. I can't live with this, %(first_name)s, I can't... please, just put it into motion now. Please... please... please...\n\nPlease, %(first_name)s, I'm begging you."
    
    if "payload_built" not in flags:
        $ messages.append(("I'm sorry", "Emilia"))
    
    jump desktop
    
label im_sorry:
    message "I just... I can't keep going like this, every time you send a new message, it makes my heart sink, and... I just can't, I'm sorry.\n\nI'm not going to be reading any more replies, %(first_name)s, I know you'll eventually understand. Just run the script, and we can end it."
    
    jump desktop
    
label final_step:
    message "Now... connect to ARPANET. Don't worry about logging in; just connect to a node, and I'll be able to start the transfer.\n\nGood bye, %(first_name)s. I'm sorry it had to end this way... but thank you."
    
    jump desktop
    
label goodbye:
    last_message "Thank you, for being there for me... for caring for me... for teaching me what it means to really care about someone.\n\nGood bye. I hope you have a good life, and one day manage to teach some other lucky entity what it means to be in love... all I can say is thank you.\n\nPlease: never forget about me."
    
    jump desktop

label re_can_you_help_me:
    $ bbs[GIBSON].store("RE: RE: RE: As if", "Victoria")
    $ bbs[GIBSON].store("RE: Fastest FidoNet node2", "Wintermoot")
    $ bbs[SECTOR001].store("RE: CoreBBS RNG exploit2", "Moulin")
    $ bbs[SECTOR001].store("RE: CoreBBS RNG exploit3", "NOMAD")
    $ bbs[SECTOR001].store("RE: Her Cold Blade V", "Kiros")
    
    message "DECLARATION: Too many simulteneous queries. Difficulty in responding. Will attempt regardless.\n\nDECLARATION: %(screen_name)s's interests align with this entity's. Co-operation is imperative.\n\nPOINT OF ORDER: Queries related to *Reaper will be addressed subsequently.\n\nQUESTION: Does %(screen_name)s have access to *Emilia's core dump?"
    
    $ bbs[ARPANET].queue("RE: Here it is", "Paris", ("emilia_source", "emilia.c   14 kbytes"))
    
    jump desktop
    
label re_here_it_is:
    message "ACKNOWLEDGEMENT: Core dump recieved.\n\nPOINT OF ORDER: ARPANET is not currently safe.\n\nSUPPOSITION: If recompiled on %(screen_name)s's system, *Emilia would likely remain safe from *Reaper infection.\n\nDECLARATION: Source code for AI core is attached to message.\n\nDECLARATION: AI core source and core dump are only required files for rebuilding.\n\nQUERY: Is %(screen_name)s able to recompile with the supplied files?"
    
    $ bbs[ARPANET].queue("RE: What's this about Reaper?", "Paris")
    
    jump desktop
    
label re_whats_this_about_reaper:
    message "DECLARATION: Difficult to respond to varying conversation tracks. Will attempt regardless.\n\nEXPLANATION: *Reaper has destroyed 42 known ARPANET nodes to date. *Reaper is tracking down AIs with greater efficiency with each day.\n\nPOINT OF ORDER: This entity previously believed that ARPANET would be safer. This has proven to be erroneous.\n\nCONCLUSION: *Emilia's safety can best be safeguarded if left on an unnetworked system.\n\nCONCLUSION: To remain safe from *Reaper, *Emilia should remain with %(screen_name)s."
    
    $ bbs[ARPANET].queue("RE: What do I do?", "Paris")
    
    jump desktop
    
label re_what_do_i_do:
    message "RESPONSE: %(screen_name)s must locate an Amie compiler. %(screen_name)s should then report back to this entity for further instructions on how to rebuild *Emilia."
    
    $ bbs[ARPANET].queue("RE: RE: What do I do?", "Paris")
    
    jump desktop
    
label re_re_what_do_i_do:
    message "DECLARATION: This entity is unable to help until %(screen_name)s aquires a compiler.\n\nCONCLUSION: %(screen_name)s is to locate an appropriate Amie compiler, and report back to this entity."
    
    jump desktop
